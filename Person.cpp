// Person Class
#include <iostream>

using namespace std;

class Person {      

  private:            
    int age;        
    string name; 

  public:          
    Person(int a = 1,string n = "default"){
           
           age = a;
           name = n;

    }

    string getName() {  
      return name;
    }
    int getAge() {  
      return age;
    }

    bool setName(string n){
       name  = n;
       return true;
    }

    bool setAge(int a){
         if((int)a > 0){
             age = a;
             return true;
         }
         return false;
    }
};

int main() {

  Person person(23,"Gurinder Batth");    // Create an object of Person Class with Parm
  // Person person();  // Create an object of Person Class 

  person.setAge(25); 
  person.setName("Gurinderpal Singh");

  cout << person.getAge() << endl;
  cout << person.getName() << endl;
  return 0;
}