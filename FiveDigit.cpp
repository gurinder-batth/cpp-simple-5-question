// Five Digit
#include <iostream>
#include <cmath>
using namespace std;

int main() {
  int iVar;

  cout << "Enter one 5 digit number: \n" ;
  cin >> iVar ;

  int length = 5;

  for(int i = length ; i > 0 ; i--){
     
     int temp = 0;

     if( length == i){

          temp = iVar / (pow(10,(i-1)));

     }else if(i == 0){

          temp = iVar % 10 ;

     }else{
          int result = iVar / (pow(10,(i-1))) ;
          temp = result % 10;
     }

      cout << temp << "\t";
  }
  cout << endl;


  return 0;
}