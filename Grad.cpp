// Question No .3
#include <iostream>
#include <vector> 

using namespace std;

class Student {      
  
  private:            
    int studentId;        
    string name; 

  public:

   string getName() {  
      return  name;
   }

   int getStudentId() {  
      return  studentId;
   }

   bool setStudentId(int id){
       studentId  = id;
       return true;
   }

   bool setName(string n){
       name  = n;
       return true;
   }

};

class Undergrad : public Student{

  private:                
    string program; 

  public:

   string getProgram() {  
      return program;
   }

   bool setProgram(string n){
       program = n;
       return true;
   }

};

class Grad : public Student{

  private:                
    string supervisor; 

  public:
   string getSupervisor() {  
      return "Student Supervisor " + supervisor;
   }

   bool setSupervisor(string n){
       supervisor = n;
       return true;
   }


};


int main() {

  vector<Grad> students; 


  for(int i = 0; i <= 5; i++){
     Grad grad; 
     grad.setStudentId(i); 
     grad.setName("Student "+ to_string(i));
     grad.setSupervisor("Supervisor "+ to_string(i));
     students.push_back(grad); 
  } 
        
  for(int i = 1; i <= students.size() - 1; i++){
      cout << "Student ID : " << students[i].getStudentId() << endl;
      cout << students[i].getName() << endl;
      cout << students[i].getSupervisor() << endl;
      cout << endl;
  }
       
  return 0;

}