#include <iostream> 
using namespace std; 
  
void decToBinary(int n) 
{ 
    int binaryNum[32]; 
  
    int i = 0; 
    while (n > 0) { 
        binaryNum[i] = n % 2; 
        n = n / 2; 
        i++; 
    } 
  
    for (int j = i - 1; j >= 0; j--) {
        cout << binaryNum[j];
    } 

    cout << endl;
} 
  
int main() 
{ 
    int n ;
    cout << "Enter the number between 0 to 255" << endl;
    cin >> n;
    decToBinary(n); 
    return 0; 
} 
