// Employee Class
#include <iostream>

using namespace std;

class Person {      

  private:            
    int age;        
    string name; 

  public:          
    Person(int a = 1,string n = "default"){
           
           age = a;
           name = n;

    }

    string getName() {  
      return name;
    }
    int getAge() {  
      return age;
    }

    bool setName(string n){
       name  = n;
       return true;
    }

    bool setAge(int a){
         if((int)a > 0){
             age = a;
             return true;
         }
         return false;
    }
};



class Employee : public Person {
      
      private:
      string company;  
    
      public:
      Employee(int a , string n ,string company);
      string getCompany(void);

};

string Employee::getCompany(void) {
   return company;
}

Employee::Employee(int a , string n ,string com = "TRU"):Person(a,n) 
{
          company = com;
}   


int main() {

  Employee employee(23,"Gurinder Batth","Brilliko"); 
  cout << employee.getAge() << endl;
  cout << employee.getName() << endl;
  cout << employee.getCompany() << endl;
  return 0;
}